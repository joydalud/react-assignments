import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import UserInput from './UserInput/UserInput';
import UserOutput from './UserOutput/UserOutput';

class App extends Component {
  state = {
    username: 'Joy'
  };

  alterUsernameHandler = (event) => {
      this.setState({
        username: event.target.value
      })
  }

  render() {
    const style = {
      width: '220px',
      height: '20px',
      margin: '15px auto',
      padding: '5px'
    };

    return (
      <div className="App">
        <UserInput changed={this.alterUsernameHandler} name={this.state.username} style={style}/>
        <UserOutput username={this.state.username} />
        <UserOutput username={this.state.username} />
      </div>
    );
  }
}

export default App;
