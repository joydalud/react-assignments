import React from 'react';
import './UserOutput.css';

const userOutput = (props) => { 
	return( 
		<div className="userOutput"> 
			<h4>{props.username}</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae lacus
			sit amet purus rutrum suscipit eget non mi. Nam fringilla ipsum elit, ut congue neque vestibulum at. Cras mattis posuere
			aliquam. Proin vitae scelerisque quam. Sed facilisis tempor accumsan. Donec viverra, arcu id fringilla condimentum,
			massa enim commodo orci, tempus varius turpis nunc eu dui. Nam ornare sed eros sit amet dignissim. Praesent eget dui
			eget metus pharetra semper. Etiam est ipsum, facilisis eget euismod nec, scelerisque non augue. </p> 
		
			<p>Etiam iaculis, leo vel pharetra malesuada, nunc nibh dictum leo, at porta diam felis eget enim. Praesent placerat 
			convallis urna vel molestie. Cras a urna sed arcu mattis fermentum sit amet quis elit. Pellentesque ut ullamcorper urna. 
			Ut in molestie ligula. Morbi ultrices, velit et tincidunt volutpat, libero est vestibulum diam, molestie pulvinar nulla 
			eros id orci. Nulla viverra elit ex, at gravida neque tincidunt non. Vivamus sed massa velit. Mauris quis porta magna. 
			Aliquam efficitur tortor in quam vulputate hendrerit. Proin eu ipsum sed velit maximus interdum. Nullam justo est, hendrerit 
			sit amet elit id, cursus dictum ex. Ut bibendum diam non ipsum hendrerit, eu porttitor ante lobortis.
			</p>
		</div> 
	) 
};


export default userOutput;
