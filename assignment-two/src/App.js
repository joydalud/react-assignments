import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import CheckLength from './CheckLength/CheckLength';
import CharComp from './CharComponent/CharComponent';


class App extends Component {
  state = {
    strtxt: ''
  };

  textChangeHandler = (event) => {
    this.setState({
      strtxt: event.target.value
    });
  }

  deleteCharHandler = (charIndex) => {
    let txtArr = this.state.strtxt;
    txtArr = txtArr.split('');
    txtArr.splice(charIndex, 1);
    txtArr = txtArr.join('');
    this.setState({strtxt: txtArr});
  }

  render() {
    let char = null;

    if(this.state.strtxt.length > 0){
      let txtArr = this.state.strtxt.split('');

      char = (
          <div>
          {
            txtArr.map((c, index) => {
              return <CharComp char={c} click={() => this.deleteCharHandler(index)} />
            })
          }
          </div>
      );
    }

    return (
      <div className="App">
        <input type="text" onChange={this.textChangeHandler} value={this.state.strtxt}/>
        <p>Text length: {this.state.strtxt.length}</p>
        <CheckLength length={this.state.strtxt.length} />
        {char}
      </div>
    );
  }
}

export default App;
