import React from 'react';

const charComponent = (props) => {
	const style = {
		display: 'inline-block',
		fontSize: '16px',
		textAlign: 'center',
		margin: '16px',
		border: '1px solid black',
		cursor: 'pointer',
		padding: '5px'
	};

	return <p onClick={props.click} style={style}>{props.char}</p>
}; 

export default charComponent;